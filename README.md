# Developer Assessment Exam

## REQUIREMENTS :
> setup is exclusive to this requirements. If any attempt on installing it on existing development environment, Please email me at roldhandasalla27@gmail.com
- Docker
- Git

### INSTALLATION :

```sh
git clone https://gitlab.com/roldhan27/company-phonebook.git project-dev
cd project dev
```

### RUN DOCKER COMPOSER
```sh
docker-compose up -d --build
```

### RUN BACKEND INSTALLATION
> To run the backend, Please run these command on the project path.
```sh
docker-compose run composer install
docker-compose exec backend bash
cp .env.example .env
npm install
npm run build
exit 
docker-compose run artisan storage:link
docker-compose run artisan key:generate
docker-compose run artisan jwt:secret #type yes
docker-compose run artisan migrate:fresh --seed

#After these commands, Visit http://localhost if the backend has shown no errors.
```

## LINKS
```sh
http://localhost:9993 #frontend
http://localhost #backend
http://localhost:8083 #phpmyadmin
```

### MySQL/PhpMyAdmin CREDENTIALS
```
user : roldhan27
password : roldhan27
root password : password0x00
```

## DEVELOPER CREDENTIALS

```
email : developer@roldhandasalla.com
password : developer
```
