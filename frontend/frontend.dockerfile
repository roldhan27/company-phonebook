FROM node:14

ARG UID
ARG GID

ENV UID=${UID}
ENV GID=${GID}

#RUN mkdir -p /var/www/app
WORKDIR /var/www/app

COPY . .

#RUN [ "npm", "install" ]
#CMD [ "npm", "run", "dev" ]

ENV NUXT_HOST=0.0.0.0

ENV NUXT_PORT=3000

EXPOSE 3000

#RUN [ "cp", ".env.example", ".env" ]
#RUN [ "npm", "install" ]
#ENTRYPOINT [ "npm", "run", "dev" ]

CMD ["/bin/sh", "-c", "cp .env.example .env && npm install && npm run dev"]

