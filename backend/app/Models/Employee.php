<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'department_id',
        'first_name',
        'middle_name',
        'last_name',
        'complete_address',
        'phone_number',
        'status'
    ];

    protected $with = [
        'company',
        'department'
    ];

    //belongs to company
    public function company(){
        return $this->belongsTo('App\Models\Companies');
    }

    //belongs to department
    public function department(){
        return $this->belongsTo('App\Models\Department');
    }

}
