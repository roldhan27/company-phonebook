<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDepartment extends Model
{
    use HasFactory;

    protected $fillable = [
        'department_id'
    ];

    //department details
    public function details(){
        return $this->belongsTo('App\Models\Department');
    }

    //belongs to user
    public function user(){
        return $this->belongsTo('App\Model\User');
    }

}
