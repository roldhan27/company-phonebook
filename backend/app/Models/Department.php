<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'description'
    ];

    protected $with = [
        'company'
    ];

    protected $appends = [
        'employeesCounts'
    ];

    //belongs to user
    public function user(){
        return $this->belongsTo('App\Models\UserDepartment');
    }

    //belongs to company
    public function company(){
        return $this->belongsTo('App\Models\Companies');
    }

    //hasMany to employees
    public function employees(){
        return $this->hasMany('App\Models\Employee');
    }

    //count of connected employees
    public function getEmployeesCountsAttribute(){
        return $this->employees()->count();
    }

}
