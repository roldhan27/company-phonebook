<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'description'
    ];

    protected $appends = [
        'departmentCounts',
        'employeesCounts'
    ];

    //has many users
    public function users(){
        return $this->hasMany('App\Models\UserCompany');
    }

    //has many departments
    public function departments(){
        return $this->hasMany('App\Models\Department','company_id');
    }

    //count of connected departments
    public function getDepartmentCountsAttribute(){
        return $this->departments()->count();
    }

    //has many employee
    public function employees(){
        return $this->hasMany('App\Models\Employee','company_id');
    }

    //count of connected employees
    public function getEmployeesCountsAttribute(){
        return $this->employees()->count();
    }

}
