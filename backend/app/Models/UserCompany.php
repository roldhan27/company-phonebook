<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCompany extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_id'
    ];

    protected $with = [
        'details'
    ];

    //belongs to company
    public function details(){
        return $this->belongsTo('App\Models\Companies','company_id');
    }

    //belongs to user
    public function user(){
        return $this->belongsTo('App\Models\User');
    }

}
