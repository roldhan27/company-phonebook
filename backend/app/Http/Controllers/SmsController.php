<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException as ME;
use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\Companies;
use App\Models\Employee;
use App\Models\SmsLogs;
use Carbon\Carbon;
use Validator;
use Exception;
use Auth;
use DB;

class SmsController extends Controller
{

    public function fetch_data(Request $req){
        $valid = Validator::make($req->all(),[
            'type'  =>  'required|string'
        ]);
        if($valid->fails()){
            return response()->json([
                'errors'    =>  $valid->errors()
            ],400);
        }
        DB::beginTransaction();
        try{
            if($req->type === 'department'){
                return response()->json([
                    'data'  =>  Department::orderBy('id', 'desc')->take(10)->get()
                ]);
            }else{
                return response()->json([
                    'data'  =>  Employee::orderBy('id', 'desc')->take(10)->get()
                ]);
            }
        }catch(Exception $e){
            DB::rollback();
            return response()->json([
                'errors'    =>  [[ $e->getMessage() ]]
            ]);
        }
    }

    public function load_more(Request $req){
        $valid = Validator::make($req->all(),[
            'type'      =>  'required|string',
            'last_id'   =>  'required|numeric'
        ]);
        if($valid->fails()){
            return response()->json([
                'errors'    =>  $valid->errors()
            ],400);
        }
        DB::beginTransaction();
        try{
            if($req->type === 'department'){
                return response()->json([
                    'data'  =>  Department::where('id','<',$req->last_id)->orderBy('id', 'desc')->take(10)->get()
                ]);
            }else{
                return response()->json([
                    'data'  =>  Employee::where('id','<',$req->last_id)->orderBy('id', 'desc')->take(10)->get()
                ]);
            }
        }catch(Exception $e){
            DB::rollback();
            return response()->json([
                'errors'    =>  [[ $e->getMessage() ]]
            ]);
        }
    }

    public function send_sms(Request $req){
        $valid = Validator::make($req->all(),[
            'type'      =>  'required|string',
            'selected'  =>  'required|array'
        ]);
        if($valid->fails()){
            return response()->json([
                'errors'    =>  $valid->errors()
            ],400);
        }
        DB::beginTransaction();
        try{
            $datas = collect($req->selected)->chunk(10);
            $names = [];
            if($req->type == 'department'){
                foreach($datas as $depts){
                    foreach($depts as $dept){
                        $d = Department::find($dept);
                        $names[] = $d->name.' ['.$d->company['name'].']';
                    }
                }
                $msg  = 'SMS has been sent to department : <br><br>';
                $msg .='<ul>';
                    foreach($names as $name){
                        $msg .= '<li>'.$name.'</li>';
                    }
                $msg .= '</ul>';
                SmsLogs::create([
                    'name'  =>  'Sent an SMS by departments - '.date('F d, Y h:i:s a',strtotime(Carbon::now())),
                    'description'   =>  $msg
                ]);
            }else{
                foreach($datas as $emps){
                    foreach($emps as $emp){
                        $e = Employee::find($emp);
                        $names[] = $e->first_name.' '.$e->middle_name.' '.$e->last_name;
                    }
                }
                $msg  = 'SMS has been sent to employees : <br><br>';
                $msg .='<ul>';
                    foreach($names as $name){
                        $msg .= '<li>'.$name.'</li>';
                    }
                $msg .= '</ul>';
                SmsLogs::create([
                    'name'  =>  'Sent an SMS by employee - '.date('F d, Y h:i:s a',strtotime(Carbon::now())),
                    'description'   =>  $msg
                ]);
            }
            DB::commit();
            return response()->json([
                'data'  =>  $msg
            ]);
        }catch(Exception $e){
            DB::rollback();
            return response()->json([
                'errors'    =>  [[ $e->getMessage() ]]
            ],400);
        }
    }

    public function send_sms_to_all(Request $req){
        $valid = Validator::make($req->all(),[
            'type'      =>  'required|string',
            'selected'  =>  'required|string'
        ]);
        if($valid->fails()){
            return response()->json([
                'errors'    =>  $valid->errors()
            ],400);
        }
        DB::beginTransaction();
        try {
            if($req->type == 'department'){
                $msg = 'An SMS has been sent to all departments.';
                SmsLogs::create([
                    'name'  =>  'Sent an SMS to all departments - '.date('F d, Y h:i:s a',strtotime(Carbon::now())),
                    'description'   =>  $msg
                ]);
            }else{
                $msg = 'An SMS has been sent to all employees.';
                SmsLogs::create([
                    'name'  =>  'Sent an SMS to all employees - '.date('F d, Y h:i:s a',strtotime(Carbon::now())),
                    'description'   =>  $msg
                ]);
            }
            DB::commit();
            return response()->json([
                'data'  =>  $msg
            ]);
        }catch(Exception $e){
            DB::rollback();
            return response()->json([
                'errors'    =>  [[ $e->getMessage() ]]
            ],400);
        }
    }

    public function logs(){
        return response()->json([
            'data'  =>  SmsLogs::orderBy('id', 'desc')->take(10)->cursor()
        ]);
    }

    public function more_logs(Request $req){
        return response()->json([
            'data'  =>  SmsLogs::where('id', '<', $req->last_id)->orderBy('id', 'desc')->take(10)->cursor()
        ]);
    }

}
