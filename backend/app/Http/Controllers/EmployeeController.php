<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException as ME;
use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\Companies;
use App\Models\Employee;
use Validator;
use Exception;
use Auth;
use DB;

class EmployeeController extends Controller
{

    public function create(Request $req){
        $valid = Validator::make($req->all(),[
            'company'           =>  'required|array',
            'department'        =>  'required|array',
            'first_name'        =>  'required|string',
            'last_name'         =>  'required|string',
            'phone_number'      =>  'required|string',
            'address'           =>  'required|string|min:10'
        ]);
        if($valid->fails()){
            return response()->json([
                'errors'    =>  $valid->errors()
            ],400);
        }
        DB::beginTransaction();
        try{
            try{
                $company = Companies::findOrFail($req->company['id']);
                //validate existence of department
                try{
                    $department = Department::findOrFail($req->department['id']);
                    //create employee
                    $company->employees()->create([
                        'department_id'         =>  $department->id,
                        'first_name'            =>  $req->first_name,
                        'middle_name'           =>  $req->middle_name,
                        'last_name'             =>  $req->last_name,
                        'phone_number'          =>  $req->phone_number,
                        'complete_address'      =>  $req->address
                    ]);
                    DB::commit();
                    return response()->json([
                        'text'  =>  'New Employee has been created.'
                    ]);
                }catch(ME $e){
                    DB::rollback();
                    return response()->json([
                        'errors'    =>  [[ 'Department not found.' ]]
                    ],400);
                }
            }catch(ME $me){
                DB::rollback();
                return response()->json([
                    'errors'    =>  [[ 'Company not found.' ]]
                ],400);
            }
        }catch(Exception $e){
            DB::rollback();
            return response()->json([
                'errors'    =>  [[$e->getMessage()]]
            ],500);
        }
    }

    public function update(Request $req){
        $valid = Validator::make($req->all(),[
            'company'           =>  'required|array',
            'department'        =>  'required|array',
            'first_name'        =>  'required|string',
            'last_name'         =>  'required|string',
            'phone_number'      =>  'required|string',
            'complete_address'  =>  'required|string|min:10'
        ]);
        if($valid->fails()){
            return response()->json([
                'errors'    =>  $valid->errors()
            ],400);
        }
        DB::beginTransaction();
        try{
            try{
                //check if company exists
                $company = Companies::findOrFail($req->company['id']);
                //check if department exists
                try{
                    $deparment = Department::findOrFail($req->department['id']);
                    //check if employee record exists
                    try{
                        $employee = Employee::findOrFail($req->id);
                        $employee->company_id = $req->company['id'];
                        $employee->department_id = $req->department['id'];
                        $employee->first_name = $req->first_name;
                        $employee->middle_name = $req->middle_name;
                        $employee->last_name = $req->last_name;
                        $employee->phone_number = $req->phone_number;
                        $employee->complete_address = $req->complete_address;
                        $employee->status = $req->status === true ? 0 : 1;
                        $employee->save();
                        DB::commit();
                        return response()->json([
                            'text'  =>  'Employee has been updated.'
                        ]);
                    }catch(ME $me){
                        DB::rollback();
                        return response()->json([
                            'errors'    =>  [ [ 'Employee not found.' ] ]
                        ],400);
                    }
                }catch(ME $me){
                    DB::rollback();
                    return response()->json([
                        'errors'    =>  [ [ 'Department not found.' ] ]
                    ],400);
                }
            }catch(ME $me){
                DB::rollback();
                return response()->json([
                    'errors'    =>  [ [ 'Company not found.' ] ]
                ],400);
            }
        }catch(Exception $e){
            DB::rollback();
            return response()->json([
                'errors'    =>  [[$e->getMessage()]]
            ],500);
        }
    }

    public function company_list(){
        return response()->json([
            'data'  =>  Companies::orderBy('id', 'desc')->cursor()
        ]);
    }

    public function departments_list(Request $req){
        try {
            $company = Companies::findOrFail($req->company_id);
            $departments = $company->departments()->orderBy('id', 'desc');
            if($departments->count()){
                return response()->json([
                    'data'  =>  $departments->cursor()
                ]);
            }else{
                return response()->json([
                    'errors' => [[ 'No departments available for this company.' ]]
                ],400);
            }
        }catch(ME $me){
            return response()->json([
                'errors' => [[ 'Company not found.' ]]
            ],400);
        }
    }

    public function list(){
        return response()->json([
            'data'  =>  Employee::orderBy('id', 'desc')->paginate(10)
        ]);
    }

    public function search(Request $req){
        return response()->json([
            'data'  =>  Employee::where('first_name','LIKE','%'.$req->keyword.'%')->orWhere('middle_name','LIKE','%'.$req->keyword.'%')->orWhere('last_name','LIKE','%'.$req->keyword.'%')->orderBy('id', 'desc')->paginate(10)
        ]);
    }

    public function delete(Request $req){
        $valid = Validator::make($req->all(),[
            'skip'  =>  'required|boolean',
            'id'    =>  'required|numeric'
        ]);
        if($valid->fails()){
            return response()->json([
                'errors'    =>  $valid->errors()
            ],400);
        }
        DB::beginTransaction();
        try{
            try{
                $employee = Employee::findOrFail($req->id);
                if($req->skip === true){
                    $employee->forceDelete();
                }else{
                    $employee->delete();
                }
                DB::commit();
                return response()->json([
                    'text'  =>  'Employee has been deleted.'
                ]);
            }catch(ME $me){
                DB::rollback();
                return response()->json([
                    'errors'    =>  [ [ 'Employee not found.' ] ]
                ],400);
            }
        }catch(Exception $e){
            DB::rollback();
            return response()->json([
                'errors'    =>  [[$e->getMessage()]]
            ],500);
        }
    }

    //trashed functions


    public function trashed_list(){
        return response()->json([
            'data'  =>  Employee::onlyTrashed()->orderBy('id', 'desc')->paginate(10)
        ]);
    }

    public function trashed_search(Request $req){
        return response()->json([
            'data'  =>  Employee::onlyTrashed()->where('first_name','LIKE','%'.$req->keyword.'%')->orWhere('middle_name','LIKE','%'.$req->keyword.'%')->orWhere('last_name','LIKE','%'.$req->keyword.'%')->orderBy('id', 'desc')->paginate(10)
        ]);
    }

    public function trashed_delete(Request $req){
        $valid = Validator::make($req->all(),[
            'id'    =>  'required|numeric'
        ]);
        if($valid->fails()){
            return response()->json([
                'errors'    =>  $valid->errors()
            ],400);
        }
        DB::beginTransaction();
        try{
            try{
                $employee = Employee::onlyTrashed()->whereId($req->id)->firstOrFail();
                $employee->forceDelete();
                DB::commit();
                return response()->json([
                    'text'  =>  'Employee has been permanently deleted.'
                ]);
            }catch(ME $me){
                DB::rollback();
                return response()->json([
                    'errors'    =>  [ [ 'Employee not found.' ] ]
                ],400);
            }
        }catch(Exception $e){
            DB::rollback();
            return response()->json([
                'errors'    =>  [[$e->getMessage()]]
            ],500);
        }
    }

    public function trashed_restore(Request $req){
        $valid = Validator::make($req->all(),[
            'id'    =>  'required|numeric'
        ]);
        if($valid->fails()){
            return response()->json([
                'errors'    =>  $valid->errors()
            ],400);
        }
        DB::beginTransaction();
        try{
            try{
                $employee = Employee::onlyTrashed()->whereId($req->id)->firstOrFail();
                $employee->restore();
                DB::commit();
                return response()->json([
                    'text'  =>  'Employee has been restored.'
                ]);
            }catch(ME $me){
                DB::rollback();
                return response()->json([
                    'errors'    =>  [ [ 'Employee not found.' ] ]
                ],400);
            }
        }catch(Exception $e){
            DB::rollback();
            return response()->json([
                'errors'    =>  [[$e->getMessage()]]
            ],500);
        }
    }

}
