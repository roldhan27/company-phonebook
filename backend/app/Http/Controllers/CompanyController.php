<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException as ME;
use Illuminate\Http\Request;
use App\Models\Companies;
use Validator;
use Exception;
use Auth;
use DB;

class CompanyController extends Controller
{

    public function create(Request $req){
        $valid = Validator::make($req->all(),[
            'name'          =>  'required|string|unique:companies',
            'description'   =>  'required|string|min:10'
        ]);
        if($valid->fails()){
            return response()->json([
                'errors'    =>  $valid->errors()
            ],400);
        }
        DB::beginTransaction();
        try{
            Companies::create([
                'name'  =>  $req->name,
                'description'   =>  $req->description
            ]);
            DB::commit();
            return response()->json([
                'text'  =>  'New Company has been created.'
            ]);
        }catch(Exception $e){
            DB::rollback();
            return response()->json([
                'errors'    =>  [[$e->getMessage()]]
            ],500);
        }
    }

    public function update(Request $req){
        $valid = Validator::make($req->all(),[
            'name'          =>  'required|string|unique:companies,name,'.$req->id,
            'description'   =>  'required|string|min:10'
        ]);
        if($valid->fails()){
            return response()->json([
                'errors'    =>  $valid->errors()
            ],400);
        }
        DB::beginTransaction();
        try{
            try{
                $company = Companies::findOrFail($req->id);
                $company->name = $req->name;
                $company->description = $req->description;
                $company->status = $req->status === true ? 0 : 1;
                $company->save();
                DB::commit();
                return response()->json([
                    'text'  =>  'Company has been updated.'
                ]);
            }catch(ME $me){
                DB::rollback();
                return response()->json([
                    'errors'    =>  [ [ 'Company not found.' ] ]
                ],400);
            }
        }catch(Exception $e){
            DB::rollback();
            return response()->json([
                'errors'    =>  [[$e->getMessage()]]
            ],500);
        }
    }

    public function list(){
        return response()->json([
            'data'  =>  Companies::orderBy('id', 'desc')->paginate(10)
        ]);
    }

    public function search(Request $req){
        return response()->json([
            'data'  =>  Companies::where('name','LIKE','%'.$req->keyword.'%')->orWhere('description','LIKE','%'.$req->keyword.'%')->orderBy('id', 'desc')->paginate(10)
        ]);
    }

    public function delete(Request $req){
        $valid = Validator::make($req->all(),[
            'skip'  =>  'required|boolean',
            'id'    =>  'required|numeric'
        ]);
        if($valid->fails()){
            return response()->json([
                'errors'    =>  $valid->errors()
            ],400);
        }
        DB::beginTransaction();
        try{
            try{
                $company = Companies::findOrFail($req->id);
                if($req->skip === true){
                    $company->forceDelete();
                }else{
                    $company->delete();
                }
                DB::commit();
                return response()->json([
                    'text'  =>  'Company has been deleted.'
                ]);
            }catch(ME $me){
                DB::rollback();
                return response()->json([
                    'errors'    =>  [ [ 'Company not found.' ] ]
                ],400);
            }
        }catch(Exception $e){
            DB::rollback();
            return response()->json([
                'errors'    =>  [[$e->getMessage()]]
            ],500);
        }
    }

    //trashed functions


    public function trashed_list(){
        return response()->json([
            'data'  =>  Companies::onlyTrashed()->orderBy('id', 'desc')->paginate(10)
        ]);
    }

    public function trashed_search(Request $req){
        return response()->json([
            'data'  =>  Companies::onlyTrashed()->where('name','LIKE','%'.$req->keyword.'%')->orWhere('description','LIKE','%'.$req->keyword.'%')->orderBy('id', 'desc')->paginate(10)
        ]);
    }

    public function trashed_delete(Request $req){
        $valid = Validator::make($req->all(),[
            'id'    =>  'required|numeric'
        ]);
        if($valid->fails()){
            return response()->json([
                'errors'    =>  $valid->errors()
            ],400);
        }
        DB::beginTransaction();
        try{
            try{
                $company = Companies::onlyTrashed()->whereId($req->id)->firstOrFail();
                $company->forceDelete();
                DB::commit();
                return response()->json([
                    'text'  =>  'Company has been permanently deleted.'
                ]);
            }catch(ME $me){
                DB::rollback();
                return response()->json([
                    'errors'    =>  [ [ 'Company not found.' ] ]
                ],400);
            }
        }catch(Exception $e){
            DB::rollback();
            return response()->json([
                'errors'    =>  [[$e->getMessage()]]
            ],500);
        }
    }

    public function trashed_restore(Request $req){
        $valid = Validator::make($req->all(),[
            'id'    =>  'required|numeric'
        ]);
        if($valid->fails()){
            return response()->json([
                'errors'    =>  $valid->errors()
            ],400);
        }
        DB::beginTransaction();
        try{
            try{
                $company = Companies::onlyTrashed()->whereId($req->id)->firstOrFail();
                $company->restore();
                DB::commit();
                return response()->json([
                    'text'  =>  'Company has been restored.'
                ]);
            }catch(ME $me){
                DB::rollback();
                return response()->json([
                    'errors'    =>  [ [ 'Company not found.' ] ]
                ],400);
            }
        }catch(Exception $e){
            DB::rollback();
            return response()->json([
                'errors'    =>  [[$e->getMessage()]]
            ],500);
        }
    }

}
