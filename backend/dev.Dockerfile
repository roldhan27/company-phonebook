FROM node:14
#FROM nginx:stable-alpine

# Create app directory
WORKDIR /var/www/app

COPY . .
COPY .env.example .env
#CMD ["sh", "-c", "copy .env.example .env"]
#RUN [ "cp", ".env.example", ".env" ]
#RUN [ "npm", "install" ]
#ENTRYPOINT [ "npm", "run", "build" ]

CMD ["/bin/sh", "-c", "cp .env.example .env && npm install && npm run build"]
ENTRYPOINT ["/bin/sh", "-c", "while :; do sleep 10; done"]
