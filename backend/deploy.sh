#!/bin/bash

cp .env.example .env

apt-get update -y && apt-get install -y libzip-dev zip git nano && docker-php-ext-install zip && docker-php-ext-install mysqli pdo pdo_mysql
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

apt-get update -y
curl -sL https://deb.nodesource.com/setup_14.x | sudo bash -
apt-get -y install nodejs
apt-get -y install npm
apt-get -y install gcc g++ make

composer install
#npm install
#npm run build
php artisan storage:link
php artisan key:generate
php artisan jwt:secret
php artisan migrate:fresh --seed
#php artisan serve --host=0.0.0.0
