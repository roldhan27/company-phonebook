<?php

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\OAuthController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\Settings\PasswordController;
use App\Http\Controllers\Settings\ProfileController;
use Illuminate\Support\Facades\Route;

//developer controllers
use App\Http\Controllers\DeveloperTools\UsersController as DT_UC;
use App\Http\Controllers\DeveloperTools\ToolsController as DT_TC;
use App\Http\Controllers\DeveloperTools\RolesController as DT_RC;
use App\Http\Controllers\DeveloperTools\PermissionsController as DT_PC;

//Company Controller
use App\Http\Controllers\CompanyController as C_CC;

//Department Controller
use App\Http\Controllers\DepartmentController as C_DC;

//Employee Controller
use App\Http\Controllers\EmployeeController as C_EC;

//SMS Controller
use App\Http\Controllers\SmsController as C_SC;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', [LoginController::class, 'logout']);

    Route::get('user', [UserController::class, 'current']);

    Route::patch('settings/profile', [ProfileController::class, 'update']);
    Route::patch('settings/password', [PasswordController::class, 'update']);

    //functions
    Route::group([ 'prefix' => 'functions'], function(){

        //company routes
        Route::group([ 'middleware' => [], 'prefix' => 'company' ], function(){
            //create
            Route::post('create', [ C_CC::class, 'create' ]);
            //list
            Route::post('list', [ C_CC::class, 'list' ]);
            //search
            Route::post('search', [ C_CC::class, 'search' ]);
            //update
            Route::post('update', [ C_CC::class, 'update' ]);
            //delete
            Route::post('delete', [ C_CC::class, 'delete' ]);
            //trashed
            Route::group([ 'prefix' => 'trashed' ], function(){
                //list
                Route::post('list', [ C_CC::class, 'trashed_list' ]);
                //search
                Route::post('search', [ C_CC::class, 'trashed_search' ]);
                //delete
                Route::post('delete', [ C_CC::class, 'trashed_delete' ]);
                //restore
                Route::post('restore', [ C_CC::class, 'trashed_restore' ]);
            });
        });

        //department routes
        Route::group([ 'middleware' => [], 'prefix' => 'departments' ], function(){
            //create
            Route::post('create', [ C_DC::class, 'create' ]);
            //list
            Route::post('list', [ C_DC::class, 'list' ]);
            //company list
            Route::post('list/companies', [ C_DC::class, 'company_list' ]);
            //search
            Route::post('search', [ C_DC::class, 'search' ]);
            //update
            Route::post('update', [ C_DC::class, 'update' ]);
            //delete
            Route::post('delete', [ C_DC::class, 'delete' ]);
            //trashed
            Route::group([ 'prefix' => 'trashed' ], function(){
                //list
                Route::post('list', [ C_DC::class, 'trashed_list' ]);
                //search
                Route::post('search', [ C_DC::class, 'trashed_search' ]);
                //delete
                Route::post('delete', [ C_DC::class, 'trashed_delete' ]);
                //restore
                Route::post('restore', [ C_DC::class, 'trashed_restore' ]);
            });
        });

        //employees routes
        Route::group([ 'middleware' => [], 'prefix' => 'employees' ], function(){
            //create
            Route::post('create', [ C_EC::class, 'create' ]);
            //list
            Route::post('list', [ C_EC::class, 'list' ]);
            //company list
            Route::post('list/companies', [ C_EC::class, 'company_list' ]);
            //department list
            Route::post('list/departments', [ C_EC::class, 'departments_list' ]);
            //search
            Route::post('search', [ C_EC::class, 'search' ]);
            //update
            Route::post('update', [ C_EC::class, 'update' ]);
            //delete
            Route::post('delete', [ C_EC::class, 'delete' ]);
            //trashed
            Route::group([ 'prefix' => 'trashed' ], function(){
                //list
                Route::post('list', [ C_EC::class, 'trashed_list' ]);
                //search
                Route::post('search', [ C_EC::class, 'trashed_search' ]);
                //delete
                Route::post('delete', [ C_EC::class, 'trashed_delete' ]);
                //restore
                Route::post('restore', [ C_EC::class, 'trashed_restore' ]);
            });
        });

        //sms routes
        Route::group([ 'middleware' => [], 'prefix' => 'sms' ], function(){
            //fetch datas
            Route::post('fetch_data', [ C_SC::class, 'fetch_data' ]);
            //load_more datas
            Route::post('load_more', [ C_SC::class, 'load_more' ]);
            //send sms
            Route::post('send_sms', [ C_SC::class, 'send_sms' ]);
            //send sms to all
            Route::post('send_sms_to_all', [ C_SC::class, 'send_sms_to_all' ]);
            //fetch logs
            Route::post('logs', [ C_SC::class, 'logs' ]);
            //fetch more logs
            Route::post('more_logs', [ C_SC::class, 'more_logs' ]);
        });

        //developer tools
        Route::group([ 'middleware' => [ 'role:Developer|Super Admin|Admin' ] ], function(){
            //fetch all roles and permissions
                //roles
                Route::post('roles/all', [ DT_TC::class, 'role_list' ])->middleware([ 'permission:can list user' ]);
                //roles
                Route::post('permissions/all', [ DT_TC::class, 'permission_list' ])->middleware([ 'permission:can list user' ]);
            //user management
            Route::group([ 'prefix' => 'users' ], function(){
                //list
                Route::post('list', [ DT_UC::class, 'init_list' ])->middleware([ 'permission:can list user' ]);
                //list
                Route::post('search', [ DT_UC::class, 'search_list' ])->middleware([ 'permission:can list user' ]);
                //create
                Route::post('create', [ DT_UC::class, 'create' ])->middleware([ 'permission:can create user' ]);
                //update
                Route::post('update', [ DT_UC::class, 'update' ])->middleware([ 'permission:can update user' ]);
                //ban process
                Route::post('ban/process', [ DT_UC::class, 'ban_process' ])->middleware([ 'permission:can update user' ]);
            });
            //role management
            Route::group([ 'prefix' => 'roles' ], function(){
                //list
                Route::post('list', [ DT_RC::class, 'init_list' ])->middleware([ 'permission:can list role' ]);
                //list
                Route::post('search', [ DT_RC::class, 'search_list' ])->middleware([ 'permission:can list role' ]);
                //create
                Route::post('create', [ DT_RC::class, 'create' ])->middleware([ 'permission:can create role' ]);
                //update
                Route::post('update', [ DT_RC::class, 'update' ])->middleware([ 'permission:can update role' ]);
                //delete
                Route::post('delete', [ DT_RC::class, 'delete' ])->middleware([ 'permission:can delete role' ]);
            });
            //permission management
            Route::group([ 'prefix' => 'permissions' ], function(){
                //list
                Route::post('list', [ DT_PC::class, 'init_list' ])->middleware([ 'permission:can list permission' ]);
                //list
                Route::post('search', [ DT_PC::class, 'search_list' ])->middleware([ 'permission:can list permission' ]);
                //create
                Route::post('create', [ DT_PC::class, 'create' ])->middleware([ 'permission:can create permission' ]);
                //update
                Route::post('update', [ DT_PC::class, 'update' ])->middleware([ 'permission:can update permission' ]);
                //delete
                Route::post('delete', [ DT_PC::class, 'delete' ])->middleware([ 'permission:can delete permission' ]);
            });
        });
    });
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', [LoginController::class, 'login']);
    Route::post('register', [RegisterController::class, 'register']);

    Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail']);
    Route::post('password/reset', [ResetPasswordController::class, 'reset']);

    Route::post('email/verify/{user}', [VerificationController::class, 'verify'])->name('verification.verify');
    Route::post('email/resend', [VerificationController::class, 'resend']);

    Route::post('oauth/{driver}', [OAuthController::class, 'redirect']);
    Route::get('oauth/{driver}/callback', [OAuthController::class, 'handleCallback'])->name('oauth.callback');
});
