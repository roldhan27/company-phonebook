<?php

namespace Database\Factories;

use App\Models\Employee;
use App\Models\Companies;
use App\Models\Department;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'company_id'            =>  $this->faker->randomElement(Companies::all())['id'],
            'department_id'         =>  $this->faker->randomElement(Department::all())['id'],
            'first_name'            =>  $this->faker->firstName(),
            'middle_name'           =>  $this->faker->lastName(),
            'last_name'             =>  $this->faker->lastName(),
            'phone_number'          =>  $this->faker->e164PhoneNumber(),
            'complete_address'      =>  $this->faker->address()
        ];
    }
}
