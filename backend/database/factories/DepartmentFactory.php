<?php

namespace Database\Factories;

use App\Models\Companies;
use App\Models\Department;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Factories\Factory;

class DepartmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'          =>  $this->faker->jobTitle(),
            'description'   =>  $this->faker->realText(50)
        ];
    }
}
