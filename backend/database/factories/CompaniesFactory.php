<?php

namespace Database\Factories;

use App\Models\Department;
use App\Models\Companies;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompaniesFactory extends Factory
{

    protected $model = Companies::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'          =>  $this->faker->company(),
            'description'   =>  $this->faker->realText(50)
        ];
    }
}
