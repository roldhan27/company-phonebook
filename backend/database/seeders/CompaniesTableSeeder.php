<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Companies;
use App\Models\Department;
use App\Models\Employee;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = Companies::factory()->count(5)->create();
        $company->each(function($c){
            $d = $c->departments()->saveMany(Department::factory()->count(5)->make());
            $d->each(function($dd) use ($c){
                $dd->employees()->saveMany(Employee::factory()->count(15)->make());
            });
        });
    }
}
