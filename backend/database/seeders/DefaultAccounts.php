<?php

namespace Database\Seeders;

use Spatie\Permission\Models\Permission;
use Illuminate\Database\Seeder;
use App\Models\User;
use Carbon\Carbon;

class DefaultAccounts extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        //create dev account
        $u = User::create([
            'name'  =>  'Developer Account',
            'email' =>  'developer@roldhandasalla.com',
            'password' => bcrypt('developer'),
            'email_verified_at' => Carbon::now()
        ]);
        //give role
        $u->assignRole('Developer');
        //give permissions
        $u->syncPermissions(Permission::all());

    }
}
